<!--
Zadatak je sljedeći:

Napraviti mini borbu dvije vojske koja se sastoji samo od programerskog dijela u PHPu bez vidljivog sučelja. 
Igra je tekstualni "rat" dvije vojske. Vojske su sačinjene od N vojnika, N se dobiva iz $_GET-a sa ?army1=50&army2=48. 
Logiku borbe, strukturu vojske, tipove podataka, strukturu klasa, strukturu fileova, ispise ako ih ima, sve dodatne feature i opcije smišljaš sam. 
Jedna vlastita implementirana ideja je obavezna (može biti bilo što, generali, random potresi, vojnici polude, baš bilo što). 
Sve je dopušteno i u principu nema ograničenja koliko mali ili veliki cijeli program mora biti. Na kraju je bitno samo da se na neki način vidi koja vojska je pobijedila i zašto. 
-->
<?php
require_once('classes/main.php');
include ("functions/CommonFunctions.php");

$game = new Game\Play();
$stats = $game->getStats();

if ($stats->first->totalStrength > $stats->second->totalStrength) echo "<h1> Pobijedila je prva vojska! </h3>";
else echo "<h1> Pobijedila je druga vojska! </h3>";

echo "<h2> Army1 </h2>";
printArmyStats($stats->first);
echo "<h5> Army structure </h5>";
printArmyStats($stats->first->structure);
echo "<h5> Army positions (Who is attacking and who is defending) </h5>";
printArmyStats($stats->first->position);
echo "<h5> Any bonuses gained because of a weather or surprises </h5>";
printArmyStats($stats->first->weather);

echo "<hr>";
echo "<h2> Army2 </h2>";
printArmyStats($stats->second);
echo "<h5> Army structure </h5>";
printArmyStats($stats->second->structure);
echo "<h5> Army positions (Who is attacking and who is defending) </h5>";
printArmyStats($stats->second->position);
echo "<h5> Any bonuses gained because of weather </h5>";
printArmyStats($stats->second->weather);
?>