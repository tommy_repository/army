<?php
namespace Game;
include "Config/config.php";
include "ArmyStructure/CalculateArmyStructure.php";
include "Weather/CalculateWeather.php";
include "Position/CalculatePosition.php";

use Game\Config as cfg;
use Game\Config\Errors as error;
use Game\ArmyStructure as ArmyStructure;
use Game\Weather as Weather;
use Game\Position as Position;

class Play{
	public $err;
    public $army;
    public $calculated;

    /* Returns array of two armies and their stats */
    public function getStats(){
        return $this->army;
    }

	public function __construct(){
        $this->init();

        /* If there is any error stop and return errors */
        if ($this->checkForErrors()) return $this->err; 

        /* Start calculating strength of armies */
        $this->fight();
        $this->calculateStrength();
    }

    public function init (){
        $this->army = new \stdClass();
    	$this->err = array();

    	if (!isset($_GET['army1']) || !isset($_GET['army2']))  array_push($this->err, error\army_not_initialized);
    }

    public function fight(){
        /*  Check the strength of armies   */
        /*
            Army structure (skill, average age, wealth, spirit level, armament)
            Positions (defending position, att position)
            Weather (visibility, surprises)
        */
        $this->army->first = new cfg\army($_GET['army1']);
        $this->army->second = new cfg\army($_GET['army2']);

        $this->calculated = new ArmyStructure\CalculateArmyStructure($this->army);
        $this->army = $this->calculated->army;

        $this->calculated = new Position\CalculatePosition($this->army);
        $this->army = $this->calculated->army;

        $this->calculated = new Weather\CalculateWeather($this->army);
        $this->army = $this->calculated->army;
    }

    public function calculateStrength(){
        $this->army->first->calculateStrength();
        $this->army->second->calculateStrength();
    }

    public function checkForErrors(){

    	if (count($this->err) > 0){
    		if (error\debug){
    			foreach($this->err as $k=>$v){
            		echo "Error: ".$v;
            		echo "<br>";
        		}
    		}
        	
        	return true;
        }

        return false;
    }
}
?>