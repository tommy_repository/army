<?php
namespace Game\Config;

class Army{
	public $size;
	public $totalStrength;
	public $structure;
	public $position;
	public $weather;

	public function __construct($size){
		$this->Init();
		$this->Set($size);
		
	}	
	

	public function set($size){
		if ($size < 0) $size = 0;

		$this->size = $size;
	}

	public function init(){
		$this->structure = new \stdClass();
		$this->position = new \stdClass();
		$this->weather = new \stdClass();
	}

	public function calculateStrength(){
		$this->structure->strength = 
			($this->structure->spirit * 2)+
			($this->structure->skill * 3)+
			($this->structure->armament + 4);

		$this->position->strength = 
			($this->position->spiritBonus * 3);

		$this->weather->strength = 
			($this->weather->spiritBonus * 2);

		$this->totalStrength = $this->structure->strength + $this->position->strength + $this->weather->strength;
		$this->totalStrength *= $this->size;
	}
}

namespace Game\Config\Errors;
/*  CONSTANTS  */
	const debug = true;
	const army_not_initialized = "Army not initialized! Please init army1 and army2.";
/* ********* */

?>