<?php
namespace Game\ArmyStructure;

class CalculateArmyStructure{
	private $currSize;
	private $army_structure;

	public $army;
	
	public function __construct($army){
		$this->set($army);

		$this->init($this->army->first->size);
		$this->calculate('first');

		$this->init($this->army->second->size);
		$this->calculate('second');
	}

	public function set($army){
		$this->army = $army;
	}

	public function init($size){
		$this->currSize = $size;
		$this->army_structure = new \stdClass();
	}

	public function calculate($army){

		/* Every stat goes from 0 to 100 except age */
		$this->calculateWealth();
		$this->calculateAge();
		$this->calculateSkill();
		$this->calculateArmament();
		$this->calculateSpiritLevel();

		switch($army){

			case 'first':
				$this->army->first->structure = $this->army_structure;
				break;

			case 'second':
				$this->army->second->structure = $this->army_structure;
				break;
		}
		

	}

	public function calculateWealth(){
		$min = rand(0,50);
		$max = rand(50,100);
		
		$this->army_structure->wealth = rand($min,$max);
	}

	public function calculateAge(){
		$wealth = $this->army_structure->wealth;

		if ($wealth > 90){
			$min = rand(30,50);
			$max = rand(50,80);
		}

		else if ($wealth > 65){
			$min = rand(24,40);
			$max = rand(40,70);
		}

		else if ($wealth > 40){
			$min = rand(20,40);
			$max = rand(40,65);
		}
		else {
			$min = rand(15,25);
			$max = rand(25,60);
		}	
		
		$this->army_structure->age = rand($min,$max);
	}

	public function calculateSkill(){
		$age = $this->army_structure->age;

		if ($age > 50){
			$min = rand(0,30);
			$max = rand(30,80);
		}

		else if ($age > 30){
			$min = rand(40,70);
			$max = rand(70,100);
		}

		else if ($age > 15){
			$min = rand(0,70);
			$max = rand(70,100);
		}

		$this->army_structure->skill = rand($min,$max);
	}

	public function calculateArmament(){
		$wealth = $this->army_structure->wealth;
		$skill = $this->army_structure->skill;
		$skillIndex = $skill/100;

		if ($wealth>90){
			$min = rand(90*$skillIndex,90*$skillIndex);
			$max = rand(90*$skillIndex,100);
		}

		else if ($wealth > 65){
			$min = rand(70*$skillIndex,80*$skillIndex);
			$max = rand(80*$skillIndex,80);
		}

		else if ($wealth > 40){
			$min = rand(50*$skillIndex,60*$skillIndex);
			$max = rand(60*$skillIndex,70);
		}
		else {
			$min = rand(15*$skillIndex,25*$skillIndex);
			$max = rand(25*$skillIndex,60);
		}	

		$this->army_structure->armament = rand($min,$max);
	}

	public function calculateSpiritLevel(){
		$wealth = $this->army_structure->wealth;
		$skill = $this->army_structure->skill;
		$age = $this->army_structure->age;
		$armament = $this->army_structure->armament;

		$skillIndex = $skill/100;
		$wealthIndex = $wealth/100;
		$armamentIndex = $armament/100;

		$index = ($skillIndex + $wealthIndex + $armamentIndex) / 3;

		$this->army_structure->spirit = ceil($index*100);
	}
}
?>