<?php
namespace Game\Weather;

class CalculateWeather{
	public $army;

	private $weatherTypes;
	private $surprises;
	private $calculate;

	public function __construct($army){
		$this->set($army);
		$this->init();

		$this->calculateWeather();
		$this->calculateSurprise();
	}

	public function set($army){
		$this->army = $army;
	}

	public function init(){
		$this->calculate = new \stdClass();
		$this->surprises = new \stdClass();

		$this->weatherTypes = array(
				"vaguely",
				"rainy",
				"sunny"
			);
		$this->surprises = array(
				"Dragon burns almost everything...",
				"Earthquake destroys almost everything...",
				"Meteors made a big mess..."
			);
	}

	public function calculateWeather(){
		$weather = rand(0,2);
		$this->army->first->weather->type = $this->army->second->weather->type = $this->weatherTypes[$weather];

		switch ($weather){
			case 0:

				if ($this->army->first->position->att) {
					$this->army->first->weather->spiritBonus = rand(40,80);
					$this->army->second->weather->spiritBonus = rand(0,60);
				}else {
					$this->army->second->weather->spiritBonus = rand(40,80);
					$this->army->first->weather->spiritBonus = rand(0,60);
				}

				break;	

			case 1:

				if ($this->army->first->position->att) {
					$this->army->first->weather->spiritBonus = rand(10,30);
					$this->army->second->weather->spiritBonus = rand(1,40);
				}
				else {
					$this->army->second->weather->spiritBonus = rand(10,30);
					$this->army->first->weather->spiritBonus = rand(1,40);
				} 

				break;

			case 2:
				$this->army->first->weather->spiritBonus = rand(1,40);
				$this->army->second->weather->spiritBonus = rand(1,40);
				break;
		}
	}

	public function calculateSurprise(){
		$surprise = rand(0,10);

		if ($surprise>=0 && $surprise<=2){
			$this->army->first->weather->surprise = $this->army->second->weather->surprise = $this->surprises[$surprise];
			$this->army->first->weather->spiritBonus = -rand(10,100);
			$this->army->second->weather->spiritBonus = -rand(10,100);
		}

	}

}

?>