<?php
namespace Game\Position;

class CalculatePosition{
	public $army;
	public $positionFirst;
	public $positionSecond;

	private $defPositions;
	private $calculate;

	public function __construct($army){
		$this->set($army);
		$this->init();

		$this->calculateWhoIsAttacker();
		$this->calculateDefPosition();
		$this->setArmyPositions();
	}

	public function set($army){
		$this->army = $army;
	}

	public function init(){
		$this->calculate = new \stdClass();
		$this->positionFirst = new \stdClass();
		$this->positionSecond = new \stdClass();

		$this->defPositions = array(
				"Open place",
				"Woods",
				"Stronghold"
			);
	}

	public function calculateWhoIsAttacker(){
		$att = rand(0,1);
 
		if ($att == 0){

			$this->calculate->att = 'first';
			$this->positionFirst->att = 1;
			$this->positionSecond->att = 0;

		}else {

			$this->calculate->att = 'second';
			$this->positionFirst->att = 0;
			$this->positionSecond->att = 1;

		}

	}

	public function calculateDefPosition(){
 		$this->calculate->position = rand(0,2);
 		$this->positionFirst->position = $this->positionSecond->position = $this->defPositions[$this->calculate->position];

		switch ($this->calculate->position){
			case 0:
				if ($this->positionFirst->att) {
					$this->positionFirst->spiritBonus = rand(60,100);
					$this->positionSecond->spiritBonus = rand(0,90);
				}else {
					$this->positionSecond->spiritBonus = rand(60,100);
					$this->positionFirst->spiritBonus = rand(0,90);
				}
				break;

			case 1:
				if ($this->positionFirst->att) {
					$this->positionFirst->spiritBonus = rand(30,40);
					$this->positionSecond->spiritBonus = rand(50,90);
				}else {
					$this->positionSecond->spiritBonus = rand(30,40);
					$this->positionFirst->spiritBonus = rand(50,90);
				}
				break;

			case 2:
				if ($this->positionFirst->att) {
					$this->positionFirst->spiritBonus = rand(0,40);
					$this->positionSecond->spiritBonus = rand(60,100);
				}else {
					$this->positionSecond->spiritBonus = rand(0,40);
					$this->positionFirst->spiritBonus = rand(60,100);
				}
				break;

		}

	}

	public function setArmyPositions(){
		$this->army->first->position = $this->positionFirst;
		$this->army->second->position = $this->positionSecond;
	}
}
?>